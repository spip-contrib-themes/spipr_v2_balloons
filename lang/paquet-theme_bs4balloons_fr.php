<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bsflatly
// Langue: fr
// Date: 07-04-2020 16:49:49
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4balloons_description' => 'Un thème dark mais coloré avec une grande photo en background',
	'theme_bs4balloons_slogan' => 'Un thème dark mais coloré avec une grande photo en background',
);
?>